<?php

namespace App\Models\Base;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\File;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class ExampleModelWithMedia extends Model implements HasMedia
{
    use HasFactory,
        SoftDeletes,
        InteractsWithMedia;

    protected $fillable = [];

    protected $casts = [];

    protected $with = ['media'];

    /**
     * See: https://spatie.be/docs/laravel-medialibrary/v9/working-with-media-collections/defining-media-collections
     */
    public function registerMediaCollections(): void
    {
        $this
            ->addMediaCollection('avatars')
            // ->acceptsFile(function (File $file) {
            //     return $file->mimeType === 'image/jpeg';
            // })
            // ->onlyKeepLatest(3)
            // ->registerMediaConversions(function (Media $media) {
            //     $this
            //         ->addMediaConversion('thumb')
            //         ->width(100)
            //         ->height(100);
            // })
            ->useFallbackUrl('/images/anonymous-user.jpg')
            ->useFallbackPath(public_path('/images/anonymous-user.jpg'))
            ->acceptsMimeTypes(['image/jpeg'])
            ->useDisk('s3')
            ->singleFile();

        // ... more collections
    }
}
