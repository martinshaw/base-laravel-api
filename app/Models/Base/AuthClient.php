<?php

namespace App\Models\Base;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class AuthClient extends Model
{
    use HasFactory,
        SoftDeletes,
        LogsActivity;

    protected $fillable = [
        'name',
        'token',
        'platform',
    ];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logAll()
            ->logExcept(['created_at', 'updated_at']);
    }

    protected $casts = [
        'name' => 'string',
        'token' => 'string',
        'platform' => 'string',
    ];

    /**
     * Get the User who created this Auth Client.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
