<?php

namespace App\Models\Base;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\LogOptions;

class AuthSession extends Model
{
    use HasFactory,
        SoftDeletes,
        LogsActivity;

    protected $fillable = [
        'personal_access_token_id',
        'last_used_ip_address',
        'restrict_to_last_ip_address',
        'last_used_user_agent',
        'restrict_to_last_user_agent',
        'invalid_after',
    ];

    protected $casts = [
        'personal_access_token_id' => 'integer',
        'last_used_ip_address' => 'string',
        'restrict_to_last_ip_address' => 'boolean',
        'last_used_user_agent' => 'string',
        'restrict_to_last_user_agent' => 'boolean',
        'invalid_after' => 'datetime',
    ];

    protected $with = [
        'personalAccessToken'
    ];

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logAll()
            ->logExcept(['created_at', 'updated_at']);
    }

    /**
     * Retrieve the model for a bound value.
     *
     * @param  mixed  $value
     * @param  string|null  $field
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function resolveRouteBinding($value, $field = null)
    {
        return $this->where('id', $value)->firstOrFail();
    }

    /**
     * Get the personal access token associated with the authentication session.
     */
    public function personalAccessToken()
    {
        return $this->belongsTo(PersonalAccessToken::class);
    }

    /**
     * Get the User who created this Auth Session.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
