<?php

namespace App\Models\Base;

use Laravel\Sanctum\PersonalAccessToken as SanctumPersonalAccessToken;

class PersonalAccessToken extends SanctumPersonalAccessToken
{
    /**
     * Get the authentication session which owns the personal access token.
     */
    public function authSession()
    {
        return $this->hasOne(AuthSession::class);
    }
}
