<?php

namespace App\Http\Requests\API\V1\Base\AuthClient;

use Illuminate\Foundation\Http\FormRequest;

class AuthClientUpdatePatchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('auth_clients.update.' . $this->route('authClient')->id);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['sometimes', 'required', 'string', 'unique:auth_clients,name'],
            'platform' => ['sometimes', 'required', 'string', 'in:web,mobile,console,desktop'],
        ];
    }
}
