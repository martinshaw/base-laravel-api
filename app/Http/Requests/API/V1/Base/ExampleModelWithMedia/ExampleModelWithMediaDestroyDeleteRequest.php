<?php

namespace App\Http\Requests\API\V1\Base\ExampleModelWithMedia;

use Illuminate\Foundation\Http\FormRequest;

class ExampleModelWithMediaDestroyDeleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('example_model_with_media.destroy.' . $this->route('example')->id);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
