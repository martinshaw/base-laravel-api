<?php

namespace App\Http\Requests\API\V1\Base\Role;

use Illuminate\Foundation\Http\FormRequest;

class RoleDestroyDeleteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('roles.destroy.' . $this->route('role')->id);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
