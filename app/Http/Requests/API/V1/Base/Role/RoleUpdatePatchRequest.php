<?php

namespace App\Http\Requests\API\V1\Base\Role;

use Illuminate\Foundation\Http\FormRequest;

class RoleUpdatePatchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('roles.update.' . $this->route('role')->id);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['sometimes', 'required', 'string', 'unique:roles,name'],
        ];
    }
}
