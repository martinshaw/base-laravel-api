<?php

namespace App\Http\Requests\API\V1\Base\User;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdatePatchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('users.update.' . $this->route('user')->id);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['sometimes', 'required', 'string'],
            'email' => ['sometimes', 'required', 'email', 'unique:users'],
            'password' => ['sometimes', 'required', 'string'],
            'locked' => ['sometimes', 'required', 'boolean'],
        ];
    }
}
