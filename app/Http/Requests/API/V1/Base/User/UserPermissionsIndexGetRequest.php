<?php

namespace App\Http\Requests\API\V1\Base\User;

use Illuminate\Foundation\Http\FormRequest;

class UserPermissionsIndexGetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('users.show.' . $this->route('user')->id . '.permissions.index');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
