<?php

namespace App\Http\Requests\API\V1\Base\AuthSession;

use Illuminate\Foundation\Http\FormRequest;

class AuthSessionShowGetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('auth_sessions.show.' . $this->route('authSession')->id);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
