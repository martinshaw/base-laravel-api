<?php

namespace App\Http\Requests\API\V1\Base\AuthSession;

use Illuminate\Foundation\Http\FormRequest;

class AuthSessionStorePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // return $this->user()->can('auth_sessions.store');

        // All guests should be able to sign in without being signed in already
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => ['required', 'string'],
            'password' => ['required', 'string'],
        ];
    }
}
