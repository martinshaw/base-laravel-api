<?php

namespace App\Http\Requests\API\V1\Base\AuthSession;

use Illuminate\Foundation\Http\FormRequest;

class AuthSessionUpdatePatchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('auth_sessions.update.' . $this->route('authSession')->id);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'last_used_ip_address' => ['sometimes', 'required', 'string', 'ip'],
            'restrict_to_last_ip_address' => ['sometimes', 'required', 'boolean'],
            'last_used_user_agent' => ['sometimes', 'required', 'nullable', 'string'],
            'restrict_to_last_user_agent' => ['sometimes', 'required', 'boolean'],
            'invalid_after' => ['sometimes', 'required', 'date_format'],
        ];
    }
}
