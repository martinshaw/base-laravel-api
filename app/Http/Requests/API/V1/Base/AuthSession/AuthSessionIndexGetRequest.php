<?php

namespace App\Http\Requests\API\V1\Base\AuthSession;

use Illuminate\Foundation\Http\FormRequest;

class AuthSessionIndexGetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('auth_sessions.index');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
