<?php

namespace App\Http\Requests\API\V1\Base\Permission;

use Illuminate\Foundation\Http\FormRequest;

class PermissionStorePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('permissions.store');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'unique:permissions,name'],
        ];
    }
}
