<?php

namespace App\Http\Requests\API\V1\Base\Permission;

use Illuminate\Foundation\Http\FormRequest;

class PermissionShowGetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('permissions.show.' . $this->route('permission')->id);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
