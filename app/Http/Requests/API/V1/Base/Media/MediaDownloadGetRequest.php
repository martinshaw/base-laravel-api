<?php

namespace App\Http\Requests\API\V1\Base\Media;

use Illuminate\Foundation\Http\FormRequest;

class MediaDownloadGetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // Note: Access restrictions are applied in controller method for this endpoint.
        //  Only records associated with 'publically available models' should be displayed to non-root users.
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
