<?php

namespace App\Http\Controllers\API\V1\Base;

use App\Http\Requests\API\V1\Base\Permission\PermissionIndexGetRequest;
use App\Http\Requests\API\V1\Base\Permission\PermissionShowGetRequest;
use App\Http\Requests\API\V1\Base\Permission\PermissionStorePostRequest;
use App\Http\Requests\API\V1\Base\Permission\PermissionUpdatePatchRequest;
use App\Http\Requests\API\V1\Base\Permission\PermissionDestroyDeleteRequest;
use Flugg\Responder\Http\MakesResponses;
use Spatie\Permission\Models\Permission;

class PermissionController extends \App\Http\Controllers\Controller
{
    use MakesResponses;

    public function index(PermissionIndexGetRequest $request)
    {
        $permissions = $request->user()->isRoot() ?
            Permission::paginate() :
            $request->users()->permissions()->paginate();

        return $this->success($permissions);
    }

    public function show(PermissionShowGetRequest $request, Permission $permission)
    {
        return $this->success($permission);
    }

    public function store(PermissionStorePostRequest $request)
    {
        $permission = Permission::create($request->only(['name']));

        return $this->success($permission);
    }

    public function update(PermissionUpdatePatchRequest $request, Permission $permission)
    {
        $permission->update($request->only(['name']));

        return $this->success($permission);
    }

    public function destroy(PermissionDestroyDeleteRequest $request, Permission $permission)
    {
        $permission->delete();

        return $this->success();
    }
}
