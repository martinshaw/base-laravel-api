<?php

namespace App\Http\Controllers\API\V1\Base;

use App\Http\Requests\API\V1\Base\Role\RoleIndexGetRequest;
use App\Http\Requests\API\V1\Base\Role\RoleShowGetRequest;
use App\Http\Requests\API\V1\Base\Role\RoleStorePostRequest;
use App\Http\Requests\API\V1\Base\Role\RoleUpdatePatchRequest;
use App\Http\Requests\API\V1\Base\Role\RoleDestroyDeleteRequest;
use Flugg\Responder\Http\MakesResponses;
use Spatie\Permission\Models\Role;

class RoleController extends \App\Http\Controllers\Controller
{
    use MakesResponses;

    public function index(RoleIndexGetRequest $request)
    {
        $roles = $request->user()->isRoot() ?
            Role::paginate() :
            $request->users()->roles()->paginate();

        return $this->success($roles);
    }

    public function show(RoleShowGetRequest $request, Role $role)
    {
        return $this->success($role);
    }

    public function store(RoleStorePostRequest $request)
    {
        $role = Role::create($request->only(['name']));

        return $this->success($role);
    }

    public function update(RoleUpdatePatchRequest $request, Role $role)
    {
        $role->update($request->only(['name']));

        return $this->success($role);
    }

    public function destroy(RoleDestroyDeleteRequest $request, Role $role)
    {
        $role->delete();

        return $this->success();
    }
}
