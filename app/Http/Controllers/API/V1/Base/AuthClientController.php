<?php

namespace App\Http\Controllers\API\V1\Base;

use App\Http\Requests\API\V1\Base\AuthClient\AuthClientIndexGetRequest;
use App\Http\Requests\API\V1\Base\AuthClient\AuthClientShowGetRequest;
use App\Http\Requests\API\V1\Base\AuthClient\AuthClientStorePostRequest;
use App\Http\Requests\API\V1\Base\AuthClient\AuthClientUpdatePatchRequest;
use App\Http\Requests\API\V1\Base\AuthClient\AuthClientDestroyDeleteRequest;
use App\Models\Base\AuthClient;
use Flugg\Responder\Http\MakesResponses;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;

class AuthClientController extends \App\Http\Controllers\Controller
{
    use MakesResponses;

    public function index(AuthClientIndexGetRequest $request)
    {
        $clients = $request->user()->isRoot() ?
            AuthClient::paginate() :
            $request->user()->authClients()->paginate();

        return $this->success($clients);
    }

    public function show(AuthClientShowGetRequest $request, AuthClient $authClient)
    {
        return $this->success($authClient);
    }

    public function store(AuthClientStorePostRequest $request)
    {
        $user = $request->user();

        $authClient = $user->authClients()->create(array_merge(
            $request->only('name', 'platform'),
            [
                'token' => Str::uuid(),
                'user_id' => $user->id,
            ]
        ));

        $user->givePermissionTo(Permission::create(['name' => 'auth_clients.*.' . $authClient->id]));

        return $this->success($authClient);
    }

    public function update(AuthClientUpdatePatchRequest $request, AuthClient $authClient)
    {
        $authClient->update($request->only([
            'name',
            'platform'
        ]));

        return $this->success($authClient);
    }

    public function destroy(AuthClientDestroyDeleteRequest $request, AuthClient $authClient)
    {
        $authClient->delete();

        return $this->success();
    }
}
