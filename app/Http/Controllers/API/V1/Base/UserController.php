<?php

namespace App\Http\Controllers\API\V1\Base;

use App\Http\Requests\API\V1\Base\User\UserIndexGetRequest;
use App\Http\Requests\API\V1\Base\User\UserMeGetRequest;
use App\Http\Requests\API\V1\Base\User\UserShowGetRequest;
use App\Http\Requests\API\V1\Base\User\UserStorePostRequest;
use App\Http\Requests\API\V1\Base\User\UserUpdatePatchRequest;
use App\Http\Requests\API\V1\Base\User\UserDestroyDeleteRequest;
use App\Http\Requests\API\V1\Base\User\UserPermissionsIndexGetRequest;
use App\Http\Requests\API\V1\Base\User\UserPermissionsAssignPostRequest;
use App\Http\Requests\API\V1\Base\User\UserPermissionsRevokeDeleteRequest;
use App\Http\Requests\API\V1\Base\User\UserRolesIndexGetRequest;
use App\Http\Requests\API\V1\Base\User\UserRolesAssignPostRequest;
use App\Http\Requests\API\V1\Base\User\UserRolesRevokeDeleteRequest;
use App\Models\Base\User;
use Flugg\Responder\Http\MakesResponses;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserController extends \App\Http\Controllers\Controller
{
    use MakesResponses;

    public function index(UserIndexGetRequest $request)
    {
        $users = $request->user()->isRoot() ?
            User::paginate() :
            User::where('id', $request->user()->id)->paginate();

        return $this->success($users);
    }

    public function me(UserMeGetRequest $request)
    {
        return $this->success($request->user());
    }

    public function show(UserShowGetRequest $request, User $user)
    {
        return $this->success($user);
    }

    public function store(UserStorePostRequest $request)
    {
        $user = User::create(
            array_merge(
                $request->only(['name', 'email', 'locked']),
                ['password' => Hash::make($request->get('password'))]
            )
        );

        $user->assignRole('general user');
        $user->givePermissionTo(Permission::create(['name' => 'users.*.' . $user->id]));

        return $this->success($user);
    }

    public function update(UserUpdatePatchRequest $request, User $user)
    {
        $user->update(
            array_merge(
                $request->only(['name', 'email', 'locked']),
                $request->has('password') ? ['password' => Hash::make($request->get('password'))] : []
            )
        );

        return $this->success($user);
    }

    public function destroy(UserDestroyDeleteRequest $request, User $user)
    {
        $user->delete();

        return $this->success();
    }

    public function getPermissions(UserPermissionsIndexGetRequest $request, User $user)
    {
        return $this->success($user->permissions()->paginate());
    }

    public function assignPermission(UserPermissionsAssignPostRequest $request, User $user, Permission $permission)
    {
        return $this->success($user->givePermissionTo($permission));
    }

    public function revokePermission(UserPermissionsRevokeDeleteRequest $request, User $user, Permission $permission)
    {
        return $this->success($user->revokePermissionTo($permission));
    }

    public function getRoles(UserRolesIndexGetRequest $request, User $user)
    {
        return $this->success($user->roles()->with('permissions')->paginate());
    }

    public function assignRole(UserRolesAssignPostRequest $request, User $user, Role $role)
    {
        return $this->success($user->assignRole($role));
    }

    public function revokeRole(UserRolesRevokeDeleteRequest $request, User $user, Role $role)
    {
        return $this->success($user->removeRole($role));
    }
}
