<?php

namespace App\Http\Controllers\API\V1\Base;

use App\Http\Requests\API\V1\Base\AuthSession\AuthSessionIndexGetRequest;
use App\Http\Requests\API\V1\Base\AuthSession\AuthSessionShowGetRequest;
use App\Http\Requests\API\V1\Base\AuthSession\AuthSessionStorePostRequest;
use App\Http\Requests\API\V1\Base\AuthSession\AuthSessionUpdatePatchRequest;
use App\Http\Requests\API\V1\Base\AuthSession\AuthSessionDestroyDeleteRequest;
use App\Models\Base\AuthClient;
use App\Models\Base\AuthSession;
use Carbon\Carbon;
use Flugg\Responder\Http\MakesResponses;
use Illuminate\Support\Facades\Auth;

class AuthSessionController extends \App\Http\Controllers\Controller
{
    use MakesResponses;

    public function index(AuthSessionIndexGetRequest $request)
    {
        $sessions = $request->user()->isRoot() ?
            AuthSession::paginate() :
            $request->user()->authSessions()->paginate();

        return $this->success($sessions);
    }

    public function show(AuthSessionShowGetRequest $request, AuthSession $authSession)
    {
        return $this->success($authSession);
    }

    public function store(AuthSessionStorePostRequest $request)
    {
        if (!Auth::attempt($request->only(['email', 'password']))) {
            return $this->error('auth_session_authenticate_bad_credentials')->respond(403);
        }

        if ($request->user()->locked && $request->user()->isRoot() === false) {
            return $this->error('auth_session_user_locked')->respond(403);
        }

        $client = AuthClient::where('token', $request->header('client-token'))->first();
        $patName = $client->name . ' - ' . $request->user()->name . ' - ' . date('Y-m-d H:i:s');

        $token = $request->user()->createToken($patName);
        $plainToken = $token->plainTextToken;
        $tokenModel = $token->accessToken;

        $authSession = $request->user()->authSessions()->create([
            'personal_access_token_id' => $tokenModel->id,
            'last_used_ip_address' => $request->ip,
            'last_used_user_agent' => $request->header('user-agent'),
            'restrict_to_last_ip_address' => false, // enabling by default would affect mobile devices which change network
            'restrict_to_last_user_agent' => true, // enabling by default to ensure one session per browser
            'invalid_after' => Carbon::now()->add('365 days')->format('Y-m-d H:i:s'),
        ]);

        return $this->success([
            'bearer_token' => $plainToken,
        ]);
    }

    public function update(AuthSessionUpdatePatchRequest $request, AuthSession $authSession)
    {
        $authSession->update($request->only([
            'last_used_ip_address',
            'restrict_to_last_ip_address',
            'last_used_user_agent',
            'restrict_to_last_user_agent',
            'invalid_after'
        ]));

        return $this->success($authSession);
    }

    public function destroy(AuthSessionDestroyDeleteRequest $request, AuthSession $authSession)
    {
        $personalAccessTokens = $authSession->personalAccessToken()->get();

        $authSession->delete();

        $personalAccessTokens->each(function ($token) {
            $token->delete();
        });

        return $this->success();
    }
}
