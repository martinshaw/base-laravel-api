<?php

namespace App\Http\Controllers\API\V1\Base;

use App\Http\Requests\API\V1\Base\ExampleModelWithMedia\ExampleModelWithMediaIndexGetRequest;
use App\Http\Requests\API\V1\Base\ExampleModelWithMedia\ExampleModelWithMediaShowGetRequest;
use App\Http\Requests\API\V1\Base\ExampleModelWithMedia\ExampleModelWithMediaStorePostRequest;
use App\Http\Requests\API\V1\Base\ExampleModelWithMedia\ExampleModelWithMediaUpdatePatchRequest;
use App\Http\Requests\API\V1\Base\ExampleModelWithMedia\ExampleModelWithMediaDestroyDeleteRequest;
use App\Models\Base\ExampleModelWithMedia;
use Flugg\Responder\Http\MakesResponses;
use Illuminate\Support\Facades\Storage;

class ExampleModelWithMediaController extends \App\Http\Controllers\Controller
{
    use MakesResponses;

    public function index(ExampleModelWithMediaIndexGetRequest $request)
    {
        return $this->success(ExampleModelWithMedia::paginate());
    }

    public function show(ExampleModelWithMediaShowGetRequest $request, ExampleModelWithMedia $example)
    {
        return $this->success($example);
    }

    public function store(ExampleModelWithMediaStorePostRequest $request)
    {
        $path = $request->file('file')->store('example_model_with_media_avatars');
        $example = ExampleModelWithMedia::create();
        $example->addMediaFromDisk($path)->toMediaCollection('avatars');

        return $this->success($example->with('media')->where('id', $example->id)->first());
    }

    public function update(ExampleModelWithMediaUpdatePatchRequest $request, ExampleModelWithMedia $example)
    {
        // Note: Collections may contain one or more files. Using multiple files would require changes to
        //  the store, update and destroy logic

        $paths = $example->getMedia('avatars')->map(function ($media) {
            return $media->getPath();
        });
        Storage::disk()->delete($paths);

        $example->clearMediaCollection('avatars');

        $path = $request->file('file')->store('example_model_with_media_avatars');
        $example->addMediaFromDisk($path)->toMediaCollection('avatars');

        return $this->success($example->with('media')->where('id', $example->id)->first());
    }

    public function destroy(ExampleModelWithMediaDestroyDeleteRequest $request, ExampleModelWithMedia $example)
    {
        $paths = $example->getMedia('avatars')->map(function ($media) {
            return $media->getPath();
        });
        Storage::disk()->delete($paths);

        $example->clearMediaCollection('avatars');

        $example->delete();

        return $this->success();
    }
}
