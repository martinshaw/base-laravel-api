<?php

namespace App\Http\Controllers\API\V1\Base;

use Flugg\Responder\Http\MakesResponses;

class TestConnectionController extends \App\Http\Controllers\Controller
{
    use MakesResponses;

    public function index()
    {
        return $this->success([
            'product_info' => config('general.api_product_url'),
            'documentation' => config('general.api_documentation_url'),
            'copyright_info' => config('general.api_copyright_url'),
            'server_time' => date("D M j G:i:s T Y"),
            'request_time_float' => $_SERVER['REQUEST_TIME_FLOAT'],
        ]);
    }
}
