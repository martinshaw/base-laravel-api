<?php

namespace App\Http\Controllers\API\V1\Base;

use App\Http\Requests\API\V1\Base\Media\MediaIndexGetRequest;
use App\Http\Requests\API\V1\Base\Media\MediaShowGetRequest;
use App\Http\Requests\API\V1\Base\Media\MediaDownloadGetRequest;
use Flugg\Responder\Http\MakesResponses;
use Illuminate\Support\Facades\Storage;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class MediaController extends \App\Http\Controllers\Controller
{
    use MakesResponses;

    public const PubliclyAvailableModels = [
        ExampleModelWithMedia::class,
    ];

    public function index(MediaIndexGetRequest $request)
    {
        $media = $request->user()->isRoot() || $request->user()->can('media.index') ?
            Media::paginate() :
            Media::where('model_type', 'in', static::PubliclyAvailableModels)->paginate();

        return $this->success($media);
    }

    public function show(MediaShowGetRequest $request, string $media)
    {
        $media = Media::where('uuid', $media)->firstOrFail();

        $shouldShow = $request->user()->can('media.show.' . $media->id) ||
            in_array($media->model_type, MediaController::PubliclyAvailableModels);

        if ($shouldShow === false) {
            return $this->error('unauthorized')->respond(403);
        }

        /** @todo: Run through transformer to remove sensitive attributes, use $hidden, or exclude 'publicly available models' from the shown results */
        return $this->success($media);
    }

    public function download(MediaDownloadGetRequest $request, string $media)
    {
        $media = Media::where('uuid', $media)->firstOrFail();

        $shouldShow = $request->user()->can('media.download.' . $media->id) ||
            in_array($media->model_type, MediaController::PubliclyAvailableModels);

        if ($shouldShow === false) {
            return $this->error('unauthorized')->respond(403);
        }

        $url = Storage::temporaryUrl(
            $media->getPath(),
            now()->addMinutes(5),
            [
                'ResponseContentType' => 'application/octet-stream',
                'ResponseContentDisposition' => 'attachment; filename=' . $media->file_name,
            ]
        );

        return response()->redirectTo($url, 201);
    }
}
