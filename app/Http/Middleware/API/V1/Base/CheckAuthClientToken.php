<?php

namespace App\Http\Middleware\API\V1\Base;

use App\Models\Base\AuthClient;
use Closure;
use Flugg\Responder\Http\MakesResponses;

class CheckAuthClientToken
{
    use MakesResponses;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $clientTokenInput = $request->header('client-token');

        if (empty($clientTokenInput)) {
            return $this->error('has_no_auth_client_token')->respond(403);
        }

        $client = AuthClient::where('token', $clientTokenInput)->first();

        if (empty($client)) {
            return $this->error('auth_client_token_invalid')->respond(403);
        }

        return $next($request);
    }
}
