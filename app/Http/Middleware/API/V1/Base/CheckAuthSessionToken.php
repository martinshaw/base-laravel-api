<?php

namespace App\Http\Middleware\API\V1\Base;

use Carbon\Carbon;
use Closure;
use Flugg\Responder\Http\MakesResponses;

class CheckAuthSessionToken
{
    use MakesResponses;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $accessToken = $request->user()->currentAccessToken();

        if (empty($accessToken)) {
            return $this->error('has_no_auth_session_token')->respond(403);
        }

        $authSession = $accessToken->authSession()->first();

        if (empty($authSession)) {
            return $this->error('has_no_auth_session_token')->respond(403);
        }

        if ($authSession->restrict_to_last_ip_address && $request->ip !== $authSession->last_used_ip_address) {
            return $this->error('auth_session_wrong_ip')->respond(403);
        }

        if ($authSession->restrict_to_last_user_agent && $request->header('user-agent') !== $authSession->last_used_user_agent) {
            return $this->error('auth_session_wrong_user_agent')->respond(403);
        }

        $expireDate = Carbon::createFromFormat('Y-m-d H:i:s', $authSession->invalid_after);
        if ($expireDate->lessThanOrEqualTo(Carbon::now())) {
            return $this->error('auth_session_expired')->respond(403);
        }

        if ($request->user()->locked && $request->user()->isRoot() === false) {
            return $this->error('auth_session_user_locked')->respond(403);
        }

        return $next($request);
    }
}
