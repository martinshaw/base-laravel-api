<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Error Message Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the Laravel Responder package.
    | When it generates error responses, it will search the messages array
    | below for any key matching the given error code for the response.
    |
    */

    'unauthenticated' => 'You are not authenticated for this request.',
    'unauthorized' => 'You are not authorized for this request. Ensure that you have sufficient permissions.',
    'page_not_found' => 'The requested page does not exist.',
    'relation_not_found' => 'The requested relation does not exist.',
    'validation_failed' => 'The given data failed to pass validation.',

    'auth_session_authenticate_bad_credentials' => 'The email or password which you have provided does not match any of our accounts.',

    'has_no_auth_client_token' => 'You must provide an Authenticated Client token for this request.',
    'auth_client_token_invalid' => 'The Authenticated Client token which you provided is invalid.',

    'has_no_auth_session_token' => 'You must provide an Authenticated Session token (JWT) for this request.',
    'auth_session_wrong_ip' => 'We were unable to complete request due to unrecognised IP address. Please disable VPNs or connect from your home WiFi network.',
    'auth_session_wrong_user_agent' => 'We were unable to complete request due to unrecognised user agent. Please use your usual web browser.',
    'auth_session_expired' => 'The Authenticated Session token which you provided is invalid. Please sign-in again.',
    'auth_session_user_locked' => 'The user account has been locked. Please try again later.'
];
