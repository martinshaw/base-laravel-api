<?php
namespace Deployer;

require 'deployer/custom-sail.php';

// Project name
set('application', 'base-laravel-api');

// Project repository
set('repository', 'git@gitlab.com:martinshaw/base-laravel-api.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys
set('shared_files', ['.env', 'docker-compose.yml']);
set('shared_dirs', ['storage']);

// Writable dirs by web server
set('writable_dirs', [
    'bootstrap/cache',
    'storage',
    'storage/app',
    'storage/app/public',
    'storage/framework',
    'storage/framework/cache',
    'storage/framework/sessions',
    'storage/framework/views',
    'storage/logs',
]);
set('allow_anonymous_stats', false);

// Hosts config
set('base_deploy_path', '/var/www');
set('http_user', 'root');
set('default_timeout', 1000);
set('keep_releases', 3);

// Hosts
host('176.58.122.224')
    ->user('root')
    ->port(22)
    ->identityFile('~/.ssh/id_rsa')
    ->forwardAgent(true)
    ->multiplexing(true)
    ->addSshOption('UserKnownHostsFile', '/dev/null')
    ->addSshOption('StrictHostKeyChecking', 'no')
    ->stage('production')
    ->roles('app')
    ->set('deploy_path', '{{base_deploy_path}}/{{application}}');

// Tasks
desc('Deploy your project');
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'custom-sail:down',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:clear_paths',
    'deploy:symlink',
    'custom-sail:install-composer-deps',
    'custom-sail:build',
    'custom-sail:up',
    'custom-sail:post-deploy',
    'deploy:unlock',
    'cleanup',
    'success'
]);

/** @todo: Need to implement rollback task for down, un-symlinking current, re-symlinking last release then up. */

desc('Sail down existing release');
task('down', [
    'deploy:lock',
    'custom-sail:down',
    'deploy:unlock',
]);

desc('Sail up existing release');
task('up', [
    'deploy:lock',
    'custom-sail:up',
    'deploy:unlock',
]);

/** @todo: Need to implement start sail queue worker as task */
/** @todo: Need to implement list sail queue workers as task */
/** @todo: Need to implement set laravel 'under construction' ON with message as a task */
/** @todo: Need to implement set laravel 'under construction' OFF as a task */

// [Optional] If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
