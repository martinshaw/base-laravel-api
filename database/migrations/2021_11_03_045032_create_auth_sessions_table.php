<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuthSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auth_sessions', function (Blueprint $table) {
            $table->id();
            $table->string('last_used_ip_address')->nullable();
            $table->boolean('restrict_to_last_ip_address')->default(true);
            $table->string('last_used_user_agent')->nullable();
            $table->boolean('restrict_to_last_user_agent')->default(true);
            $table->timestamp('invalid_after')->nullable();

            $table->unsignedBigInteger('personal_access_token_id');
            $table->foreign('personal_access_token_id')->references('id')->on('personal_access_tokens');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auth_sessions');
    }
}
