<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;
use Illuminate\Support\Str;

class PermissionsInitialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Setup initial roles and permissions
         */

        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // Create permissions
        Permission::create(['name' => 'users.index']);
        // Permission::create(['name' => 'users.show.*']);
        // Permission::create(['name' => 'users.create']);
        // Permission::create(['name' => 'users.update.*']);
        // Permission::create(['name' => 'users.destroy.*']);

        $rootUserRole = Role::create(['name' => 'root user']);
        // gets all permissions via Gate::after rule; see AuthServiceProvider

        $generalUserRole = Role::create(['name' => 'general user']);
        $generalUserRole->givePermissionTo('users.index');
        // $generalUserRole->givePermissionTo('users.show.*');
        // $generalUserRole->givePermissionTo('users.create');
        // $generalUserRole->givePermissionTo('users.update.*');
        // $generalUserRole->givePermissionTo('users.destroy.*');


        /**
         * Setup first user as Root with all permissions
         */
        $rootUserEmail = 'hello+root@martinshaw.co';
        $rootUserPassword = Str::uuid();

        $rootUser = \App\Models\Base\User::create([
            'name' => 'Root User',
            'email' => $rootUserEmail,
            'password' => Hash::make($rootUserPassword),
            'locked' => 0,
        ]);
        $rootUser->assignRole($rootUserRole);

        $this->command->info(
            "Created Root User '$rootUserEmail' with password '$rootUserPassword'.\n" .
            "Ensure that this plain-text password is stored and kept securely.\n"
        );


        /**
         * Setup API Auth Client for testing purposes
         */
        if (config('app.env') === 'local') {
            $testAuthClientName = 'Postman Testing Client';
            $testAuthClientToken = Str::uuid();

            $testAuthClient = $rootUser->authClients()->create([
                'name' => $testAuthClientName,
                'token' => $testAuthClientToken,
                'platform' => 'console',
                'user_id' => $rootUser->id,
            ]);

            $this->command->info(
                "Created Test Auth Client '$testAuthClientName' with token '$testAuthClientToken'.\n" .
                "Ensure that this token is stored in your API client.\n" .
                "Delete this token immediately if this seeder is running in production!\n"
            );
        }
    }
}
