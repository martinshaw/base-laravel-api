<?php
namespace Deployer;

require 'recipe/common.php';

task('custom-sail:down', function () {
    try {
        run('ls {{current_path}}/vendor'); // throws exception if vendor folder doesn't exist
        run('cd {{current_path}} && ./vendor/bin/sail down');
    } catch (\Exception $e) {
        //
    }
});

task('custom-sail:build', function () {
    run("cd {{release_path}} && ./vendor/bin/sail build");
});

task('custom-sail:up', function () {
    run("cd {{release_path}} && chmod -R 755 ./storage/logs/");
    run("cd {{release_path}} && ./vendor/bin/sail up -d");
});

task('custom-sail:install-composer-deps', function () {
    try {
        run('ls {{release_path}}/vendor'); // throws exception if vendor folder doesn't exist
    } catch (\Exception $e) {
        run('cd {{release_path}} && docker run --rm -u "$(id -u):$(id -g)" -v $(pwd):/var/www/html -w /var/www/html laravelsail/php80-composer:latest composer install --ignore-platform-reqs');
    }
});

task('custom-sail:post-deploy', function () {
    run('cd {{release_path}} && docker exec $(docker ps -a | grep "sail-8.0" |  cut -d" " -f1) composer du -o');
    run('cd {{release_path}} && docker exec $(docker ps -a | grep "sail-8.0" |  cut -d" " -f1) php artisan config:cache');
    run('cd {{release_path}} && docker exec $(docker ps -a | grep "sail-8.0" |  cut -d" " -f1) php artisan route:cache');
    run('cd {{release_path}} && docker exec $(docker ps -a | grep "sail-8.0" |  cut -d" " -f1) php artisan view:cache');
    run('cd {{release_path}} && docker exec $(docker ps -a | grep "sail-8.0" |  cut -d" " -f1) php artisan migrate');
});
    // ->local()
    // ->shallow()
    // ->setPrivate();
