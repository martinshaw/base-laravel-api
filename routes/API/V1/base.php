<?php

use App\Http\Controllers\API\V1\Base\TestConnectionController;
use App\Http\Controllers\API\V1\Base\UserController;
use App\Http\Controllers\API\V1\Base\AuthSessionController;
use App\Http\Controllers\API\V1\Base\AuthClientController;
use App\Http\Controllers\API\V1\Base\PermissionController;
use App\Http\Controllers\API\V1\Base\RoleController;
use App\Http\Controllers\API\V1\Base\ExampleModelWithMediaController;
use App\Http\Controllers\API\V1\Base\MediaController;
use App\Http\Middleware\API\V1\Base\CheckAuthClientToken;
use App\Http\Middleware\API\V1\Base\CheckAuthSessionToken;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')
    ->middleware([
        CheckAuthClientToken::class,
    ])
    ->group(function () {

        Route::get('/', [ TestConnectionController::class, 'index' ]);

        Route::post('/users', [ UserController::class, 'store' ]); // Register

        Route::post('/auth-sessions', [ AuthSessionController::class, 'store' ]); // Sign In

        Route::middleware([
                'auth:sanctum',
                CheckAuthSessionToken::class,
            ])
            ->group(function () {

                Route::get('/users', [ UserController::class, 'index' ]);
                Route::get('/users/me', [ UserController::class, 'me' ]);
                Route::get('/users/{user}', [ UserController::class, 'show' ]);
                Route::patch('/users/{user}', [ UserController::class, 'update' ]);
                Route::delete('/users/{user}', [ UserController::class, 'destroy' ]);

                Route::get('/users/{user}/permissions', [ UserController::class, 'getPermissions' ]);
                Route::post('/users/{user}/permissions/{permission}', [ UserController::class, 'assignPermission' ]);
                Route::delete('/users/{user}/permissions/{permission}', [ UserController::class, 'revokePermission' ]);
                Route::get('/users/{user}/roles', [ UserController::class, 'getRoles' ]);
                Route::post('/users/{user}/roles/{role}', [ UserController::class, 'assignRole' ]);
                Route::delete('/users/{user}/roles/{role}', [ UserController::class, 'revokeRole' ]);

                Route::get('/auth-clients', [ AuthClientController::class, 'index' ]);
                Route::get('/auth-clients/{authClient}', [ AuthClientController::class, 'show' ]);
                Route::post('/auth-clients', [ AuthClientController::class, 'store' ]);
                Route::patch('/auth-clients/{authClient}', [ AuthClientController::class, 'update' ]);
                Route::delete('/auth-clients/{authClient}', [ AuthClientController::class, 'destroy' ]);

                Route::get('/auth-sessions', [ AuthSessionController::class, 'index' ]);
                Route::get('/auth-sessions/{authSession}', [ AuthSessionController::class, 'show' ]);
                Route::patch('/auth-sessions/{authSession}', [ AuthSessionController::class, 'update' ]);
                Route::delete('/auth-sessions/{authSession}', [ AuthSessionController::class, 'destroy' ]);

                Route::get('/permissions', [ PermissionController::class, 'index' ]);
                Route::get('/permissions/{permission}', [ PermissionController::class, 'show' ]);
                Route::post('/permissions', [ PermissionController::class, 'store' ]);
                Route::patch('/permissions/{permission}', [ PermissionController::class, 'update' ]);
                Route::delete('/permissions/{permission}', [ PermissionController::class, 'destroy' ]);

                Route::get('/roles', [ RoleController::class, 'index' ]);
                Route::get('/roles/{role}', [ RoleController::class, 'show' ]);
                Route::post('/roles', [ RoleController::class, 'store' ]);
                Route::patch('/roles/{role}', [ RoleController::class, 'update' ]);
                Route::delete('/roles/{role}', [ RoleController::class, 'destroy' ]);

                Route::get('/example-model-with-media', [ ExampleModelWithMediaController::class, 'index' ]);
                Route::get('/example-model-with-media/{example}', [ ExampleModelWithMediaController::class, 'show' ]);
                Route::post('/example-model-with-media', [ ExampleModelWithMediaController::class, 'store' ]);
                Route::patch('/example-model-with-media/{example}', [ ExampleModelWithMediaController::class, 'update' ]);
                Route::delete('/example-model-with-media/{example}', [ ExampleModelWithMediaController::class, 'destroy' ]);

                Route::get('/media', [ MediaController::class, 'index' ]);
                Route::get('/media/{media}', [ MediaController::class, 'show' ]);
                Route::get('/media/{media}/download', [ MediaController::class, 'download' ]);

            });

    });
