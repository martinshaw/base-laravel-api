<?php

return [

    'api_product_url' => env('API_PRODUCT_URL', 'http://localhost/product'),

    'api_documentation_url' => env('API_DOCUMENTATION_URL', 'http://localhost/docs'),

    'api_copyright_url' => env('API_COPYRIGHT_URL', 'http://localhost/docs'),

];
