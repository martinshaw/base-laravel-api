## Base Laravel API
### Current base boilerplate for use as foundation of Laravel API + Next.js (React Native, etc...), MySQL projects.
*By Martin Shaw*

*Copyright 2021, Martin Shaw. All creative rights reserved relating to first party project code and intellectual property*


# Installation

1. Pull this project `base-laravel-api` as the template of your new repository for the backend API server.

2. As the root user on your system, add host file entry for this local service: `echo "\n127.0.0.1     api.base-laravel-api.dev" >> /etc/hosts`, replacing 'base-laravel-api' with the name of your project.

3. Copy the environmental variables template into a new '.env' file: `cp .env.example .env`, and replace values including the `APP_URL` appropriately.

4. If this is the first time installing this project, you will need to install Laravel Sail dependencies: 

```bash
docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v $(pwd):/var/www/html \
    -w /var/www/html \
    laravelsail/php80-composer:latest \
    composer install --ignore-platform-reqs
```

5. Run the local Laravel Sail server: `./vendor/bin/sail up`, and navigate to the chosen URL's index route (e.g. `http://api.base-laravel-api.dev/`) in your API client, observing a successful 200 response.

6. Install the client for the Minio local object storage server: `brew install minio/stable/mc`.

7. Set the server alias for the Minio client before creating the local testing bucket: `mc alias set minio http://localhost:9002`.

8. Create the bucket in the Minio client: `mc mb minio/local`.

9. If you are running without pre-existing DB data and/or in development, you should run the following command to run all migrations fresh and to seed permissions/roles:

```bash
./vendor/bin/sail artisan migrate:fresh --seed --seeder=PermissionsInitialSeeder
```


# Endpoints / Routes

Contact Martin (developer@martinshaw.co) for access to the Collection of endpoints for the Postman API client.
